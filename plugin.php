<?php
/**
 * Plugin name: SF Cookie-Layer
 * Description:       Cookie-Layer
 * Version:           1.0.0
 * Author:            Mario Klug <mario.klug@sourcefactory.at>
 * Text Domain:       sf-cookie-layer
 */

use SFCookieConsent\AbstractFeature;

if ( ! defined( 'WPINC' ) ) {
    die;
}

require_once __DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'AbstractFeature.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'ContentBlocker.php';

foreach (glob(__DIR__ . DIRECTORY_SEPARATOR . 'features' . DIRECTORY_SEPARATOR . '*.php') as $file) {
    require_once $file;
}

require_once __DIR__ . DIRECTORY_SEPARATOR . 'settings' . DIRECTORY_SEPARATOR . 'init.php';

add_action('init', function() {
    wp_enqueue_script('sf-cookie-layer', plugin_dir_url(__FILE__) . '/dist/main.js', [], filemtime(__DIR__ . '/dist/main.js'), true);
    wp_enqueue_style('sf-cookie-layer', plugin_dir_url(__FILE__) . '/dist/main.css');

    wp_set_script_translations('sf-cookie-layer', 'sf-cookie-layer');
});

function sf_cookie_consent_register_feature($id, AbstractFeature $feature) {
    add_filter('sf_cookie_content_registered_features', function ($features) use ($id, $feature) {
        $features[$id] = $feature;
        return $features;
    });
}

class SFCookieConsentConfig {
    public $title = null;
    public $message = null;
    public $privacyMessage = null;
    public $settingsMessage = null;
    public $buttons = [
        'essential' => null,
        'all' => null,
        'settings' => null,
        'apply' => null,
        'back' => null,
        'details' => null,
    ];

    /** @var AbstractFeature[] */
    public $features = [];

    public $customFeatures = [];

    public $privacyLink;
    public $currentLink;

    public $autoDetected = [];

    public function __construct()
    {
        $this->features = apply_filters('sf_cookie_content_registered_features', []);

        $this->title = __('Diese Webseite verwendet Cookies', 'sf-cookie-layer');
        $this->message = __('Bitte wählen Sie welche Cookies sie erlauben möchten.', 'sf-cookie-layer');
        $this->buttons['essential'] = __('Essentielle Cookies erlauben', 'sf-cookie-layer');
        $this->buttons['all'] = __('Alle Cookies erlauben', 'sf-cookie-layer');
        $this->buttons['settings'] = __('Einstellungen', 'sf-cookie-layer');
        $this->buttons['apply'] = __('Anwenden', 'sf-cookie-layer');
        $this->buttons['back'] = __('Zurück', 'sf-cookie-layer');
        $this->buttons['details'] = __('Details', 'sf-cookie-layer');
        $this->settingsMessage = __('Hier können Sie ihre präferierten Einstellungen festlegen.', 'sf-cookie-layer');
        $this->privacyMessage = __('Weitere Informationen finden Sie in unserer <a href="[link]">Datenschutzerklärung</a>', 'sf-cookie-layer');
    }

    /**
     * @param null $title
     */
    public function setTitle($title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param null $message
     */
    public function setMessage($message): self
    {
        $this->message = $message;
        return $this;
    }

    public function setPrivacyPage($privacyLink, $currentLink): self {
        $this->privacyLink = $privacyLink;
        $this->currentLink = $currentLink;
        return $this;
    }

    public function setButtons(string $essential, string $all): self
    {
        $this->buttons['essential'] = $essential;
        $this->buttons['all'] = $all;
        return $this;
    }

    public function setPrivacyMessage(string $message): self
    {
        $this->privacyMessage = $message;
        return $this;
    }

	public function setSettingsMessage( ?string $settingsMessage ): void {
		$this->settingsMessage = $settingsMessage;
	}

    public function enableAnalytics(string $script): self
    {
        $this->features['google-analytics']['enabled'] = true;
        $this->features['google-analytics']['script'] = $script;
        return $this;
    }

    public function enable(string $id, string $script)
    {
        if (!isset($this->features[$id])) {
            throw new Exception("unknown feature \"{$id}\"");
        }

        $feature = $this->features[$id];

        $this->enableCustomFeature($feature->getId(), $feature->isEssential(), $feature->getName(), $feature->getPrivacyHeadline(), $feature->getPrivacyContent(), $script, $feature->getJsCallback());
    }

    public function enableCustomFeature(string $identifier, bool $required, string $name, string $privacyHeadline, string $privacyContent, string $script = null, string $jsCallback = null)
    {
        $search = array_filter($this->customFeatures, function ($f) use ($identifier) {
            return $f['identifier'] === $identifier;
        });

        if (!$search) {
            $this->customFeatures[] = [
                'identifier' => $identifier,
                'required' => $required,
                'name' => $name,
                'privacyHeadline' => $privacyHeadline,
                'privacyContent' => $privacyContent,
                'script' => $script,
                'jsCallback' => $jsCallback,
            ];
        }
    }

    public function addAutoDetected(string $code, string $name): self
    {
        $this->autoDetected[$code] = $name;
        return $this;
    }
}

add_filter('the_content', function ($content) {
    /** @var SFCookieConsentConfig $config */
    $config = apply_filters('sf_cookie_consent_config', new SFCookieConsentConfig());

    foreach ($config->features as $feature) {
        foreach ($feature->getBlockers() as $blockerConfig) {
            $blocker = $blockerConfig['callback']($content);

            if ($blocker) {
                $width = $blocker->width ? $blocker->width . 'px' : '100%';
                $height = $blocker->height ? $blocker->height . 'px' : '100%';
                $config = htmlspecialchars($blocker->content);

                $blockHtml = trim(<<<EOF
<div class="sf-cookie-consent-blocked no-consent" data-consent-id="{$feature->getId()}" style="width: {$width}; height: {$height};">
    <div class="config" style="display: none">{$config}</div>
    <div class="no-consent" style="display: none">
        {$blockerConfig['message']}<br>
        <a href="javascript:void(0)" class="sf-reopen-consent button sf-button sf-button-black">Einstellungen erneut öffnen</a>    
    </div>
    <div class="consent-container"></div>
</div>
EOF
);
                $content = str_replace($blocker->content, $blockHtml, $content);
            }
        }
    }

    return $content;
}, 10);

add_action('get_footer', function() {
    $config = apply_filters('sf_cookie_consent_config', new SFCookieConsentConfig());
?>
    <script>
        //window.addEventListener('load', () => cookieConsent.config(<?//= json_encode($config) ?>//).run())
        window.cookieConsentConfig = <?= json_encode($config) ?>
    </script>

    <div id="sf-cookie-consent-container"></div>
<?php
}, 100);
