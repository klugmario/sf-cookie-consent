<?php
namespace SFCookieConsent;

class ContentBlocker
{
    public $content;
    public $width;
    public $height;

    public function __construct(string $content, $width = null, $height = null)
    {
        $this->content = $content;
        $this->width = $width;
        $this->height = $height;
    }
}
