<?php
namespace SFCookieConsent;

abstract class AbstractFeature
{
    private $id;
    private $essential;

    private $blockers = [];

    public function __construct(string $id, bool $essential = false)
    {
        $this->id = $id;
        $this->essential = $essential;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isEssential(): bool
    {
        return $this->essential;
    }

    public function registerBlocker(callable $callback, string $blockMessage)
    {
        $this->blockers[] = [
            'message' => $blockMessage,
            'callback' => $callback
        ];
    }

    /**
     * @return array
     */
    public function getBlockers(): array
    {
        return $this->blockers;
    }

    abstract function getName(): string;
    abstract function getPrivacyHeadline(): string;
    abstract function getPrivacyContent(): string;
    abstract function getJsCallback(): string;
}
