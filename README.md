## Available filters

Customization is possible by adding filters to themes functions.php.

```php
<?php
add_filter('sf_cookie_consent_config', function(SFCookieConsentConfig $config) {
    return $config
        ->setPrivacyPage($privacyLink, $currentLink)
        ->setPrivacyMessage(__('Weitere Informationen finden Sie in unserer <a href="[link]">Datenschutzerklärung</a>'))
        ->setTitle('We use cookies')
        ->setMessage('Some explanation text')
        ->setButtons('Allow essential', 'Allow all');
});
```

`setPrivacyPage()` expects links to the privacy- and the current page. The layer is not
shown if both links are the same.

The privacy message is only shown if the link is set. `[link]` is a placeholder and gets
automatically replaced.

### Open layer manually

The layer object registers itself as `cookieConsent` inside the window object.

To start it providing a callback function call `window.cookieConsent.prompt(callback)`.
Otherwise call `window.cookieConsent.showLayer()`.