<?php

use SFCookieConsent\ContentBlocker;

define('SF_COOKIE_CONSENT_YOUTUBE', 'youtube');

class YoutubeFeature extends \SFCookieConsent\AbstractFeature
{
    public function __construct()
    {
        parent::__construct(SF_COOKIE_CONSENT_YOUTUBE, true);

        $this->registerBlocker(function ($content) {
            preg_match_all('/<iframe(.+)src="https:\/\/(|www\.)youtube\.com\/embed\/(.+)"(.+)><\/iframe>/Uis', $content, $matches);
            foreach ($matches[0] as $tag) {
                $tmp = explode(' ', $tag);
                $sizeAttributes = array_filter($tmp, function ($t) {
                    return substr($t, 0, 7) === 'width="' || substr($t, 0, 8) === 'height="';
                });
                $sizes = [
                    'width' => null,
                    'height' => null,
                ];
                foreach ($sizeAttributes as $attr) {
                    $tmp = explode('=', $attr);
                    if (count($tmp) === 2) {
                        list ($key, $value) = $tmp;
                        if (!in_array($key, ['width', 'height'])) {
                            continue;
                        }
                        $value = intval(trim($value, '\'"'));
                        if ($value) {
                            $sizes[$key] = $value;
                        }
                    }
                }

                return new ContentBlocker($tag, $sizes['width'], $sizes['height']);
            }

            return null;
        }, __('Sie haben dem Laden von Youtube-Videos nicht zugestimmt wodurch dieses Video nicht angezeigt werden kann.'));
    }

    function getName(): string
    {
        return __('Youtube', 'sf-cookie-consent');
    }

    function getPrivacyHeadline(): string
    {
        return __('Einbinden von Youtube Videos', 'sf-cookie-consent');
    }

    function getPrivacyContent(): string
    {
        return trim(__("Unsere Seite verwendet für die Einbindung von Videos den Anbieter YouTube LLC , 901 Cherry Avenue, San Bruno, CA 94066, USA, vertreten durch Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA. Normalerweise wird bereits bei Aufruf einer Seite mit eingebetteten Videos Ihre IP-Adresse an YouTube gesendet und Cookies auf Ihrem Rechner installiert. Wir haben unsere YouTube-Videos jedoch mit dem erweiterten Datenschutzmodus eingebunden (in diesem Fall nimmt YouTube immer noch Kontakt zu dem Dienst Double Klick von Google auf, doch werden dabei laut der Datenschutzerklärung von Google personenbezogene Daten nicht ausgewertet). Dadurch werden von YouTube keine Informationen über die Besucher mehr gespeichert, es sei denn, sie sehen sich das Video an. Wenn Sie das Video anklicken, wird Ihre IP-Adresse an YouTube übermittelt und YouTube erfährt, dass Sie das Video angesehen haben. Sind Sie bei YouTube eingeloggt, wird diese Information auch Ihrem Benutzerkonto zugeordnet (dies können Sie verhindern, indem Sie sich vor dem Aufrufen des Videos bei YouTube ausloggen).
<br><br>
Von der dann möglichen Erhebung und Verwendung Ihrer Daten durch YouTube haben wir keine Kenntnis und darauf auch keinen Einfluss. Nähere Informationen können Sie der Datenschutzerklärung von YouTube  unter <a href='https://www.google.de/intl/de/policies/privacy/' target='_blank'>https://www.google.de/intl/de/policies/privacy/</a> entnehmen. Zudem verweisen wir für den generellen Umgang mit und die Deaktivierung von Cookies auf unsere allgemeine Darstellung in dieser Datenschutzerklärung.", 'sf-cookie-consent'));
    }

    function getJsCallback(): string
    {
        return '';
    }
}

sf_cookie_consent_register_feature(SF_COOKIE_CONSENT_YOUTUBE, new YoutubeFeature());
