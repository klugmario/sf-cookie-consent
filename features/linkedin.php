<?php
define('SF_COOKIE_CONSENT_LINKEDIN', 'linkedin');

class LinkedInFeature extends \SFCookieConsent\AbstractFeature
{
    public function __construct()
    {
        parent::__construct(SF_COOKIE_CONSENT_LINKEDIN, true);
    }

    function getName(): string
    {
        return __('LinkedIn', 'sf-cookie-consent');
    }

    function getPrivacyHeadline(): string
    {
        return __('Nutzung von LinkedIn', 'sf-cookie-consent');
    }

    function getPrivacyContent(): string
    {
        return trim(__("...", 'sf-cookie-consent'));
    }

    function getJsCallback(): string
    {
        return '';
    }
}

sf_cookie_consent_register_feature(SF_COOKIE_CONSENT_LINKEDIN, new LinkedInFeature());
