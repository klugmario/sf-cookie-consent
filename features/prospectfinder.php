<?php
define('SF_COOKIE_CONSENT_PROSPECT_FINDER', 'prospectfinder');

class ProspectFinderFeature extends \SFCookieConsent\AbstractFeature
{
    public function __construct()
    {
        parent::__construct(SF_COOKIE_CONSENT_PROSPECT_FINDER, true);
    }

    function getName(): string
    {
        return __('Prospect Finder', 'sf-cookie-consent');
    }

    function getPrivacyHeadline(): string
    {
        return __('Nutzung von Prospect Finder', 'sf-cookie-consent');
    }

    function getPrivacyContent(): string
    {
        return trim(__("...", 'sf-cookie-consent'));
    }

    function getJsCallback(): string
    {
        return '';
    }
}

sf_cookie_consent_register_feature(SF_COOKIE_CONSENT_PROSPECT_FINDER, new ProspectFinderFeature());
