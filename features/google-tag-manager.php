<?php
define('SF_COOKIE_CONSENT_GOOGLE_TAG_MANGER', 'google-tag-manager');

class GoogleTagManagerFeature extends \SFCookieConsent\AbstractFeature
{
    public function __construct()
    {
        parent::__construct(SF_COOKIE_CONSENT_GOOGLE_TAG_MANGER, true);
    }

    function getName(): string
    {
        return __('Google Tag Manager', 'sf-cookie-consent');
    }

    function getPrivacyHeadline(): string
    {
        return __('Nutzung von Google Tag Manager', 'sf-cookie-consent');
    }

    function getPrivacyContent(): string
    {
        return trim(__("Wir verwenden den Dienst namens Google Tag Manager von Google. \"Google\" ist eine Firmengruppe und besteht aus den Firmen Google Ireland Ltd. (Anbieter des Dienstes), Gordon House, Barrow Street, Dublin 4, Irland sowie Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA sowie andere verbundene Unternehmen der Google LLC.
<br><br>
Wir haben einen Auftragsverarbeitungsvertrag mit Google abgeschlossen. Der Google Tag Manager ist ein Hilfsdienst und verarbeitet selbst personenbezogenen Daten nur zu technisch notwendigen Zwecken. Der Google Tag Manager sorgt für das Laden anderer Komponenten, die ihrerseits unter Umständen Daten erfassen. Der Google Tag Manager greift nicht auf diese Daten zu.
<br><br>
Weitere Informationen zum Google Tag Manager finden Sie in den <a href='https://www.google.de/intl/de/policies/privacy/' target='_blank'>Datenschutzbestimmungen von Google</a>.
<br><br>
Bitte beachten Sie, dass amerikanische Behörden, etwa Geheimdienste, aufgrund amerikanischer Gesetze wie dem Cloud Act möglicherweise Zugriff auf personenbezogene Daten erhalten könnten, die beim Einbinden dieses Dienstes zwangsläufig aufgrund des Internet Protokolls (TCP) mit Google ausgetauscht werden.
", 'sf-cookie-consent'));
    }

    function getJsCallback(): string
    {
        return '';
    }
}

sf_cookie_consent_register_feature(SF_COOKIE_CONSENT_GOOGLE_TAG_MANGER, new GoogleTagManagerFeature());
