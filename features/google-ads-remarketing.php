<?php
define('SF_COOKIE_CONSENT_GOOGLE_ADDS_REMARKETING', 'google-ads-remarketing');

class GoogleAdsRemarketingFeature extends \SFCookieConsent\AbstractFeature
{
    public function __construct()
    {
        parent::__construct(SF_COOKIE_CONSENT_GOOGLE_ADDS_REMARKETING, true);
    }

    function getName(): string
    {
        return __('Google Ads Remarketing', 'sf-cookie-consent');
    }

    function getPrivacyHeadline(): string
    {
        return __('Nutzung von Google Ads Remarketing', 'sf-cookie-consent');
    }

    function getPrivacyContent(): string
    {
        return trim(__("...", 'sf-cookie-consent'));
    }

    function getJsCallback(): string
    {
        return '';
    }
}

sf_cookie_consent_register_feature(SF_COOKIE_CONSENT_GOOGLE_ADDS_REMARKETING, new GoogleAdsRemarketingFeature());
