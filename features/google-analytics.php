<?php
define('SF_COOKIE_CONSENT_ANALYTICS', 'google-analytics');

class GoogleAnalyticsFeature extends \SFCookieConsent\AbstractFeature
{
    public function __construct()
    {
        parent::__construct(SF_COOKIE_CONSENT_ANALYTICS, true);
    }

    function getName(): string
    {
        return __('Google Analytics', 'sf-cookie-consent');
    }

    function getPrivacyHeadline(): string
    {
        return __('Nutzung von Google Analytics', 'sf-cookie-consent');
    }

    function getPrivacyContent(): string
    {
        return trim(__("Wir verwenden Google Analytics, um die Website-Nutzung zu analysieren. Die daraus gewonnenen Daten werden genutzt, um unsere Website sowie Werbemaßnahmen zu optimieren.<br> 
<br>
Google Analytics ist ein Webanalysedienst, der von Google Inc. (1600 Amphitheatre Parkway, Mountain View, CA 94043, United States) betrieben und bereitgestellt wird. Google verarbeitet die Daten zur Website-Nutzung in unserem Auftrag und verpflichtet sich vertraglich zu Maßnahmen, um die Vertraulichkeit der verarbeiteten Daten zu gewährleisten.<br>
<br>
Während Ihres Website-Besuchs werden u.a. folgende Daten aufgezeichnet:<br>
<ul>
    <li>Aufgerufene Seiten</li>
    <li>Bestellungen inkl. des Umsatzes und der bestellten Produkte</li>
    <li>Die Erreichung von \"Website-Zielen\" (z.B. Kontaktanfragen und Newsletter-Anmeldungen)</li>
    <li>Ihr Verhalten auf den Seiten (beispielsweise Verweildauer, Klicks, Scrollverhalten)</li>
    <li>Ihr ungefährer Standort (Land und Stadt)</li>
    <li>Ihre IP-Adresse (in gekürzter Form, sodass keine eindeutige Zuordnung möglich ist)</li>
    <li>Technische Informationen wie Browser, Internetanbieter, Endgerät und Bildschirmauflösung</li>
    <li>Herkunftsquelle Ihres Besuchs (d.h. über welche Website bzw. über welches Werbemittel Sie zu uns gekommen sind)</li>
</ul>
Diese Daten werden an Server von Google in den USA übertragen. Wir weisen darauf hin, dass in den USA datenschutzrechtlich nicht das gleiche Schutzniveau wie innerhalb der EU garantiert werden kann.<br> 
<br>
Google Analytics speichert Cookies in Ihrem Webbrowser für die Dauer von zwei Jahren seit Ihrem letzten Besuch. Diese Cookies enthaltene eine zufallsgenerierte User-ID, mit der Sie bei zukünftigen Website-Besuchen wiedererkannt werden können.<br>
<br>
Die aufgezeichneten Daten werden zusammen mit der zufallsgenerierten User-ID gespeichert, was die Auswertung pseudonymer Nutzerprofile ermöglicht. Diese nutzerbezogenen Daten werden automatisch nach 14 Monaten gelöscht. Sonstige Daten bleiben in aggregierter Form unbefristet gespeichert.<br>
<br>
Sollten Sie mit der Erfassung nicht einverstanden sein, können Sie diese mit der einmaligen Installation des <a href=\"https://tools.google.com/dlpage/gaoptout?hl=de\" target=\"_blank\">Browser-Add-ons zur Deaktivierung von Google Analytics</a> unterbinden oder durch das Ablehnen der Cookies über unseren Cookie Einstellungs Dialog.", 'sf-cookie-consent'));
    }

    function getJsCallback(): string
    {
        return 'sfCookieConsentAnalyticsCallback';
    }
}

sf_cookie_consent_register_feature(SF_COOKIE_CONSENT_ANALYTICS, new GoogleAnalyticsFeature());

add_filter('get_footer', function() {
    ?>
    <script>
    function sfCookieConsentAnalyticsCallback(config) {
        // const matches = config.script.match(/gtag\('config', '(.*)'\);/)
        // if (matches) {
        //     const id = matches.pop()
        //     const scriptTag = document.createElement('script')
        //     scriptTag.src = `https://www.googletagmanager.com/gtag/js?id=${id}`
        //     scriptTag.onload = function() {
        //         window.dataLayer = window.dataLayer || [];
        //         function gtag(){dataLayer.push(arguments);}
        //         gtag('js', new Date());
        //         gtag('config',id);
        //     }
        //     document.body.appendChild(scriptTag)
        // }
        return false;
    }
    </script>
    <?php
});
