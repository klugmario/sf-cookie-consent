<?php
define('SF_COOKIE_CONSENT_LEADFEEDER', 'leadfeeder');

class LeadFeederFeature extends \SFCookieConsent\AbstractFeature
{
    public function __construct()
    {
        parent::__construct(SF_COOKIE_CONSENT_LEADFEEDER, true);
    }

    function getName(): string
    {
        return __('Leadfeader Tracker', 'sf-cookie-consent');
    }

    function getPrivacyHeadline(): string
    {
        return __('Nutzung von Leadfeader', 'sf-cookie-consent');
    }

    function getPrivacyContent(): string
    {
        return trim(__("Dies ist ein Marketing-Service. Dieser Service hilft dabei, Leads zu generieren, indem analysiert wird, welche Unternehmen bestimmte Websites besucht haben.<br>
<br>
<b>Verarbeitungsunternehmen</b><br>
<br>
Liidio Oy / Leadfeeder<br>
Mikonkatu 17 C Helsinki, Finnland<br>
<br>
<b>Datenzwecke</b><br>
<br>
Diese Liste enthält die Zwecke der Datenerfassung und -verarbeitung. Eine Einwilligung gilt nur für die angegebenen Zwecke. Die gesammelten Daten dürfen nicht für andere Zwecke als die unten aufgeführten Zwecke verwendet oder gespeichert werden.<br>
<ul>
    <li>Leadgenerierung</li>
    <li>Besucheranalyse</li>
</ul>
<b>Verwendete Technologien</b>
<ul>
    <li>1st Party Cookies</li>
    <li>IP Tracking</li>
</ul>
<b>Gesammelte Daten</b><br>
<br>
Diese Liste enthält alle (persönlichen) Daten, die durch die Nutzung dieses Dienstes gesammelt werden.<br>
<ul>
    <li>Besuchte Websites</li>
    <li>Datum und Uhrzeit des Besuchs</li>
    <li>IP Adresse</li>
    <li>Referrer URL</li>
    <li>Browserinformationen</li>
    <li>Gerätebetriebssystem</li>
    <li>Geräteinformation</li>
    <li>Domainname</li>
</ul>
<b>Rechtliche Grundlage</b><br>
<br>
Im Folgenden wird die erforderliche Rechtsgrundlage für die Datenverarbeitung aufgeführt.<br>
Art. 6 Abs. 1 s. 1 lit. f DSGVO<br>
<br>
<b>Ort der Verarbeitung</b><br>
<br>
Europäische Union<br>
<br>
<b>Frist der Datenspeicherung</b><br>
<br>
Die Frist der Datenspeicherung ist die Zeitspanne, in der die gesammelten Daten für die angegebenen Verarbeitungszwecke gespeichert werden. Die Daten werden gelöscht, sobald sie für die angegebenen Verarbeitungszwecke nicht mehr benötigt werden.<br>
<br>
<b>Datenempfänger</b>
<br>
<ul>
    <li>Leadfeeder</li>
    <li>Drittanbieter</li>
</ul>
<br>
<b>Datenschutzbeauftragter des Verarbeitungsunternehmens</b><br>
<br>
Nachfolgend finden Sie die E-Mail-Adresse des Datenschutzbeauftragten des Verarbeitungsunternehmens.<br>
<br>
privacy@leadfeeder.com<br>
<br>
Klicken Sie hier, um die Datenschutzbestimmungen des Datenverarbeiters zu lesen. <a href='https://www.leadfeeder.com/privacy/' target='_blank'>https://www.leadfeeder.com/privacy/</a>.", 'sf-cookie-consent'));
    }

    function getJsCallback(): string
    {
        return '';
    }
}

sf_cookie_consent_register_feature(SF_COOKIE_CONSENT_LEADFEEDER, new LeadFeederFeature());
