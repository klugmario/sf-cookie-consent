import '../scss/style.scss'
import GoogleAnalytics from "./features/GoogleAnalytics";
import Feature from "./features/Feature";
import __ from '@wordpress/i18n'

class SFCookieConsent
{
    layer = null

    generalCallbacks = []
    callback = null

    conf = {
        title: __('Diese Webseite verwendet Cookies', 'sf-cookie-layer'),
        message: __('Bitte wählen Sie, welche Cookies sie erlauben möchten.', 'sf-cookie-layer'),
        privacyMessage: __('Weitere Informationen finden Sie in unserer <a href="[link]">Datenschutzerklärung</a>', 'sf-cookie-layer'),
        settingsMessage: __('Hier können Sie ihre präferierten Einstellungen festlegen.', 'sf-cookie-layer'),
        buttons: {
            essential: __('Essentielle Cookies erlauben', 'sf-cookie-layer'),
            settings: __('Einstellungen', 'sf-cookie-layer'),
            all: __('Alle Cookies erlauben', 'sf-cookie-layer'),
            apply: __('Anwenden', 'sf-cookie-layer'),
            back: __('Zurück', 'sf-cookie-layer'),
            details: __('Details', 'sf-cookie-layer'),
        },
        privacyLink: null,
        currentLink: null,
        features: {
            'google-analytics': {
                enabled: false,
                script: null
            }
        }
    }

    config(config) {
        this.conf = { ...this.conf, ...config }
        return this
    }

    addGeneralCallback(callback) {
        this.generalCallbacks.push(callback)
        return this
    }

    run() {
        if ( ! this.layer) {
            this.layer = this.buildLayer()
        }

        if ( ! this.isChecked()) {
            this.showLayer()
        }
    }

    setChecked(allow) {
        localStorage.setItem('sf-cookies-checked', '1')
        localStorage.setItem('sf-cookies-allowed', '' + (allow ? 1 : 0))

        if (this.callback) {
            this.callback()
            this.callback = null
        }

        this.generalCallbacks.forEach(callback => callback())

        this.hideLayer()
    }

    isChecked() {
        return localStorage.getItem('sf-cookies-checked') && parseInt(localStorage.getItem('sf-cookies-checked')) > 0
    }

    isAllowed() {
        return localStorage.getItem('sf-cookies-allowed') && parseInt(localStorage.getItem('sf-cookies-allowed')) > 0
    }

    prompt(callback) {
        this.callback = callback
        this.showLayer()
    }

    showLayer() {
        if (this.conf.privacyLink && this.conf.currentLink && this.conf.privacyLink === this.conf.currentLink) {
            return
        }

        this.layer.classList.add('shown');
    }

    hideLayer() {
        this.layer.classList.remove('shown');
    }

    getFeatures(): Feature[] {
        const features = []
        if (this.conf.features['google-analytics'].enabled) {
            features.push(new GoogleAnalytics(this.conf.features['google-analytics'].script))
        }
        return features
    }

    hasFeatures() {
        return !!this.getFeatures().length
    }

    buildLayer() {
        const wrapper = this.e('div', [], { id: 'sf-cookie-consent' })
        const layer = this.e('div', ['layer'])
        const header = this.e('header', [], { text: this.conf.title })
        const defaultBody = this.e('p', ['body-default'], { html: this.conf.message })
        const buttons = this.e('div', ['buttons'])

        const btnAll = this.e('a', ['button', 'sf-button', 'sf-button-black'], { text: this.conf.buttons.all })
        const btnEssential = this.e('a', ['button', 'sf-button', 'sf-button-white'], { text: this.conf.buttons.essential })
        const btnSettings = this.hasFeatures() ? this.e('a', ['button', 'sf-button', 'sf-button-white'], { text: this.conf.buttons.settings }) : null

        layer.appendChild(header)
        layer.appendChild(defaultBody)

        buttons.appendChild(btnAll)
        buttons.appendChild(btnEssential)

        if (btnSettings) {
            buttons.appendChild(btnSettings)

            const ul = this.e('ul')
            for (const feature of this.getFeatures()) {
                const hl = this.e('header', [], { text: feature.getName() })
                const detailsLink = this.e('a', [], { text: __('Details', 'sf-cookie-layer') })

                const li = this.e('li')
                li.appendChild(hl)
                li.appendChild(detailsLink)

                ul.appendChild(li)
            }

            const settingsBody = this.e('p', ['body-settings'])
            settingsBody.appendChild(this.e('p', [], { html: this.conf.settingsMessage }))
            settingsBody.appendChild(ul)

            btnSettings.addEventListener('click', () => wrapper.classList.add('settings'))

            layer.appendChild(settingsBody)
        }

        if (this.conf.privacyLink) {
            const privacy = this.e('p', ['privacy'])
            layer.appendChild(privacy)
        }

        layer.appendChild(buttons)

        const backdrop = this.e('div', ['backdrop'])
        backdrop.classList.add('backdrop')

        wrapper.appendChild(backdrop)
        wrapper.appendChild(layer)

        document.body.appendChild(wrapper)

        btnAll.addEventListener('click', () => {
            this.setChecked(true)
        })

        btnEssential.addEventListener('click', () => {
            this.setChecked(false)
        })

        return wrapper
    }

    e(tag, classNames = [], attributes: { [key:string]: any } = {}) {
        const el = document.createElement(tag)
        for (const className of classNames) {
            el.classList.add(className)
        }
        for (const key in attributes) {
            if (key === 'html') {
                el.innerHTML = attributes[key]
                continue
            }
            if (key === 'text') {
                el.innerText = attributes[key]
                continue
            }
            el.setAttribute(key, attributes[key])
        }
        return el
    }
}

// @ts-ignore
window.cookieConsent = new SFCookieConsent()

// @ts-ignore
window.areCookiesAllowed = cookieConsent.isAllowed
// @ts-ignore
window.openCookieConsentLayer = callback => cookieConsent.prompt(callback)
