const decodeHtml = (html) => {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

const checkBlockedContent = (allowedFeatures) => {
    document.querySelectorAll('.sf-cookie-consent-blocked').forEach(container => {
        const noConsentContainer = container.querySelector('.no-consent')
        const contentContainer = container.querySelector('.consent-container')
        const html = decodeHtml(container.querySelector('.config').innerHTML)

        const consentId = container.getAttribute('data-consent-id')

        if (!window['areCookiesAllowed'](consentId)) {
            noConsentContainer.style.display = 'block'
            contentContainer.style.display = 'none'
            contentContainer.innerHTML = ''
        } else {
            contentContainer.innerHTML = html
            noConsentContainer.style.display = 'none'
            contentContainer.style.display = 'block'
        }
    })
}

export default checkBlockedContent
