import Feature from "./Feature";

export default class GeneralFeature extends Feature {
    private identifier: string;
    private essential: boolean;
    private name: string;
    private privacyHeadline: string;
    private privacyContent: string;
    private script: string;
    private jsCallback: string|null;

    constructor(identifier: string, essential: boolean, name: string, privacyHeadline: string, privacyContent: string, script: string = null, jsCallback: string = null) {
        super();
        this.identifier = identifier;
        this.essential = essential
        this.name = name;
        this.privacyHeadline = privacyHeadline;
        this.privacyContent = privacyContent;
        this.script = script;
        this.jsCallback = jsCallback;
    }

    getIdentifier(): string {
        return this.identifier
    }

    isEssential(): boolean {
        return this.essential
    }

    getName(): string {
        return this.name
    }

    getPrivacyHeadline(): string {
        return this.privacyHeadline
    }

    getPrivacyContent(): string {
        return this.privacyContent
    }

    callback() {
        if (!this.script) {
            return
        }

        if (this.jsCallback) {
            if (typeof window[this.jsCallback] === 'undefined') {
                console.error(`cookie consent js callback "${this.jsCallback}" does not exist`)
                return
            }
            return window[this.jsCallback](this)
        }

        const matches = this.script.match(/^<script>([\s\S]+)<\/script>/)
        if (matches) {
            const script = matches[1]
            const scriptTag = document.createElement('script')
            scriptTag.innerHTML = script
            document.body.appendChild(scriptTag)
        }
    }
}
