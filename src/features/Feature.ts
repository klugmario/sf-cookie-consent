export default abstract class Feature {
    abstract getIdentifier(): string
    abstract isEssential(): boolean
    abstract getName(): string
    abstract getPrivacyHeadline(): string
    abstract getPrivacyContent(): string
    abstract callback(): void
}
