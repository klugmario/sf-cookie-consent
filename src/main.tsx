import * as React from "react";
import { createRoot } from 'react-dom/client';
import Layer from "./Layer";

const container = document.getElementById('sf-cookie-consent-container')

const root = createRoot(container)
root.render(<Layer/>)
