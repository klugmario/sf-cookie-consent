import * as React from "react";
import {useEffect, useState} from "react";
import GeneralFeature from "./features/GeneralFeature";
import Feature from "./features/Feature";
import { __ } from '@wordpress/i18n'
import checkBlockedContent from "./lib/checkBlockedContent";

import '../scss/style.scss'

declare global {
    interface Window {
        cookieConsent: {
            allowedFeatures: string[]
            callbacks: { [feature: string]: Function[] }
            addGeneralCallback(feature: string, callback: Function): void
            isAllowed(identifier: string): boolean
            showLayer(): void
            hasFeature(identifier: string): boolean
            addFeature(identifier: string, essential: boolean, name: string, privacyHeadline: string, privacyContent: string)
            features: Feature[]
        }
        areCookiesAllowed(identifier: string): boolean
        openCookieConsentLayer(): void
        cookieConsentConfig: {
            title: string
            message: string
            privacyMessage: string
            settingsMessage: string
            buttons: {
                essential: string
                settings: string
                all: string
                apply: string
                back: string
                details: string
            }
            privacyLink: string
            currentLink: null
            features: {
                'google-analytics': {
                    enabled: boolean,
                    script: string
                }
            },
            customFeatures: GeneralFeature[]
        }
    }
}

export default function() {
    const [visible, setVisible] = useState<boolean>(false)
    const [showSettings, setShowSettings] = useState<boolean>(false)
    const [features, setFeatures] = useState<Feature[]>([])
    const [showFeatureDetails, setShowFeatureDetails] = useState<Feature>(null)
    const [checkedFeatures, setCheckedFeatures] = useState<string[]>([])

    const [config, setConfig] = useState(window.cookieConsentConfig)

    useEffect(() => {
        const features = []

        config.customFeatures.forEach(f => {
            features.push(new GeneralFeature(f.identifier, f.essential, f.name, f.privacyHeadline, f.privacyContent, f.script, f.jsCallback))
        })

        let allowedFeatures = localStorage.getItem('sf-cookies-allowed') ? JSON.parse(localStorage.getItem('sf-cookies-allowed')) : []
        if (typeof allowedFeatures !== 'object') {
            allowedFeatures = []
        }

        setFeatures(features)
        setCheckedFeatures(allowedFeatures)

        window.cookieConsent = {
            allowedFeatures,
            callbacks: {},
            features,
            addGeneralCallback: (feature, callback) => {
                if (typeof window.cookieConsent.callbacks[feature] === "undefined") {
                    window.cookieConsent.callbacks[feature] = []
                }
                window.cookieConsent.callbacks[feature].push(callback)
            },
            isAllowed: feature => isAllowed(feature),
            showLayer: () => setVisible(true),
            hasFeature: feature => Object.keys(features).indexOf(feature) >= 0,
            addFeature: (identifier: string, essential: boolean, name: string, privacyHeadline: string, privacyContent: string, script: string = null) => {
                if (window.cookieConsent.features.filter(f => f.getIdentifier() === identifier).length === 0) {
                    window.cookieConsent.features.push(new GeneralFeature(identifier, essential, name, privacyHeadline, privacyContent, script))
                }
                setFeatures([...window.cookieConsent.features])
            }
        }

        window.areCookiesAllowed = identifier => window.cookieConsent.isAllowed(identifier)
        window.openCookieConsentLayer = () => setVisible(true)

        features.forEach(f => {
            window.cookieConsent.addGeneralCallback(f.getIdentifier(), () => f.callback())
        })

        allowedFeatures.forEach(i => {
            fireCallback(i)
        })

        checkBlockedContent(allowedFeatures)

        document.querySelectorAll('.sf-reopen-consent').forEach(btn => btn.addEventListener('click', () => window['openCookieConsentLayer']()))
    }, [])

    const isChecked = () => localStorage.getItem('sf-cookies-checked') && parseInt(localStorage.getItem('sf-cookies-checked')) > 0
    const isAllowed = feature => window.cookieConsent.allowedFeatures?.indexOf(feature) >= 0

    const toggleCheckedFeature = (identifier: string) => {
        const newCheckedFeatures = checkedFeatures.includes(identifier)
            ? checkedFeatures.filter(i => i !== identifier)
            : [...checkedFeatures, identifier]

        setCheckedFeatures(newCheckedFeatures)
    }

    const fireCallback = (identifier: string) => {
        if (typeof window.cookieConsent.callbacks[identifier] !== "undefined") {
            window.cookieConsent.callbacks[identifier].forEach(cb => cb())
        }
    }

    const allowAll = (onlyEssential: boolean) => {
        const checkedFeatures = window.cookieConsent.features
            .filter(f => onlyEssential ? f.isEssential() : true)
            .map(f => f.getIdentifier())

        localStorage.setItem('sf-cookies-checked', '1')
        localStorage.setItem('sf-cookies-allowed', JSON.stringify(checkedFeatures))

        window.cookieConsent.allowedFeatures = checkedFeatures

        checkedFeatures.forEach(feature => fireCallback(feature))
        checkBlockedContent(checkedFeatures)

        setVisible(false)

        var dataLayer = window.dataLayer = window.dataLayer || [];
        dataLayer.push({"event":"consent_update"});
    }

    const allowChecked = () => {
        localStorage.setItem('sf-cookies-checked', '1')
        localStorage.setItem('sf-cookies-allowed', JSON.stringify(checkedFeatures))

        window.cookieConsent.allowedFeatures = checkedFeatures

        checkedFeatures.forEach(feature => fireCallback(feature))
        checkBlockedContent(checkedFeatures)

        setVisible(false)

        var dataLayer = window.dataLayer = window.dataLayer || [];
        dataLayer.push({"event":"consent_update"});
    }

    const isPrivacyPage = () => config.privacyLink && config.currentLink && config.privacyLink === config.currentLink

    if (!isPrivacyPage() && !isChecked() && !visible) {
        setVisible(true)
    }

    const classNames = []
    if (visible) {
        classNames.push('shown')
    }

    return (<div id={'sf-cookie-consent'} className={classNames.join(' ')}>
        <div className="backdrop"></div>

        {!showFeatureDetails &&
            <div className="layer">
                <header>{ config.title }</header>

                {!showSettings &&
                    <div>
                        <p className="body">
                            {config.message}
                        </p>
                        {config.privacyLink && <p className="privacy" dangerouslySetInnerHTML={{
                            __html: config.privacyMessage.replace('[link]', config.privacyLink)
                        }}></p>}
                        <div className="buttons">
                            <a className="button sf-button sf-button-black" onClick={() => allowAll(false)}>
                                {config.buttons.all}
                            </a>
                            <a className="button sf-button sf-button-white" onClick={() => allowAll(true)}>
                                {config.buttons.essential}
                            </a>
                            {features.length &&
                                <a className="button sf-button sf-button-white" onClick={() => setShowSettings(true)}>
                                    {config.buttons.settings}
                                </a>}
                        </div>
                    </div>
                ||
                    <div>
                        <div className="body">
                            <p>{config.settingsMessage}</p>

                            <ul className="features">
                                {features.map(f => (
                                    <li key={f.getIdentifier()}>
                                        <input type="checkbox" id={`sf-cookie-consent-chk-${f.getIdentifier()}`} checked={checkedFeatures.includes(f.getIdentifier())} onChange={() => toggleCheckedFeature(f.getIdentifier())}/>
                                        <label htmlFor={`sf-cookie-consent-chk-${f.getIdentifier()}`}>{f.getName()}</label>
                                        <br/>
                                        <a onClick={() => setShowFeatureDetails(f)}>
                                            {config.buttons.details}
                                        </a>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <div className="buttons">
                            <a className="button sf-button sf-button-black" onClick={() => allowChecked()}>
                                {config.buttons.apply}
                            </a>
                            <a className="button sf-button sf-button-white" onClick={() => setShowSettings(false)}>
                                {config.buttons.back}
                            </a>
                        </div>
                    </div>
                }
            </div>
        ||
            <div className="layer feature-privacy">
                <header>{showFeatureDetails.getPrivacyHeadline()}</header>
                <p dangerouslySetInnerHTML={{ __html: showFeatureDetails.getPrivacyContent() }}/>
                <div className="buttons">
                    <a className="button sf-button sf-button-white" onClick={() => setShowFeatureDetails(null)}>
                        {config.buttons.back}
                    </a>
                </div>
            </div>
        }
    </div>)
}
