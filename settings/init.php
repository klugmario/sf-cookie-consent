<?php
$domain = 'sf-cookie-consent';

add_action( 'admin_menu', function() use ($domain) {
    add_menu_page(
            __('Cookie Banner', $domain),
            __('Cookie Banner', $domain),
            'manage_options',
            'sf-cookie-consent',
            'sf_cookie_consent_settings',
            'dashicons-saved'
    );
});

/*
function register_cookie_consent_settings() {
    register_setting('sf-maps', 'sf_maps_api_key', [
        'type' => 'string',
        'show_in_rest' => true,
        'sanitize_callback' => 'sanitize_text_field',
    ]);

    register_setting('sf-maps', 'sf_maps_config', [
        'type' => 'string',
        'show_in_rest' => true
    ]);
}
*/

//add_action( 'admin_init', 'register_cookie_consent_settings');

add_action('admin_init', function() {
    if ( ! session_id()) {
        session_start();
    }

    if (sf_cookie_consent_is_settings_page()) {
        wp_enqueue_script('plugin-settings', plugin_dir_url(__FILE__) . '/js/settings-page.js');
    }
});

function sf_cookie_consent_is_settings_page() {
    return 'admin.php' === basename($_SERVER['SCRIPT_FILENAME'])
        && isset($_GET['page']) && 'sf-cookie-consent' === $_GET['page'];
}

/*
add_action( 'admin_notices', function () {
    if (sf_cookie_consent_is_settings_page()) {
        if (isset($_SESSION['PLUGIN_SF_MAPS_SETTINGS_SAVED'])) {
            unset($_SESSION['PLUGIN_SF_MAPS_SETTINGS_SAVED']);
            ?>
            <div class="notice notice-info is-dismissible">
                <p>
                    Änderungen gespeichert!
                </p>
            </div>
            <?php
        }
        return;
    }
});
*/

function sf_cookie_consent_settings() {
    global $domain;
    require __DIR__ . DIRECTORY_SEPARATOR . 'settings-page.php';
}
