<?php
/** @var string $domain */

/** @var SFCookieConsentConfig $config */
$config = apply_filters('sf_cookie_consent_config', new SFCookieConsentConfig());
?>

<div class="wrap">
    <h1><?= __('Cookie Banner', $domain) ?></h1>

    <?php if ($config->autoDetected): ?>
        <h2><?= __('Automatisch erkanne Plugins', $domain) ?></h2>

        <?= __('Die folgenden Plugins wurden automatisch erkannt und dem Cookie Banner hinzugefügt', $domain) ?>:

        <ul>
            <?php foreach ($config->autoDetected as $name): ?>
            <li><?= $name ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif ?>
    <pre>
        <?php print_r($config) ?>
    </pre>
</div>
